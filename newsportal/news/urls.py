from django.urls import path
from .views import Posts, PostDetail, PostDeleteView, PostCreateView, PostUpdateView

urlpatterns = [
   path('', Posts.as_view()),
   path('<int:pk>', PostDetail.as_view(), name='new'),
   path('add/<int:pk>', PostCreateView.as_view(), name='new_add'),
   path('delete/<int:pk>', PostDeleteView.as_view(), name='new_delete'),
   path('add/<int:pk>', PostUpdateView.as_view(), name='post_update'),
]