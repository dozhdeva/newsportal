from django import template


register = template.Library()

CENC = ['человек', 'задач']


@register.filter()
def censor(text):
   for word in text.split():
      if word.strip('.,"/') in CENC:
         word = (word[:1]) + ('*' * (len(word) - 1))
      text_censor = f'{word}'
   return text_censor