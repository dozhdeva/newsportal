from django.views.generic import ListView, DetailView, DeleteView, UpdateView, CreateView
from django.core.paginator import Paginator
from .models import Post
from .forms import PostForm


class Posts(ListView):
    model = Post
    ordering = ['-dateCreation']
    template_name = 'news.html'
    context_object_name = 'posts'
    paginate_by = 3

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['value1'] = f"Все новости на портале -- {Post.objects.all().count()}"
        return context

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)

        if form.is_valid():
            form.save()

        return super().get(request, *args, **kwargs)

class PostDetail(DetailView):
    template_name = 'tamplates/new.html'
    queryset = Post.objects.all()


class PostCreateView(CreateView):
    template_name = 'templates/new_add.html'
    form_class = PostForm

   def get_object(self,**kwargs):
        id = self.kwargs.get('pk')
        return Post.objects.get(pk=id)


class PostDeleteView(DeleteView):
    template_name = 'templates/new_delete.html'
    queryset = Post.objects.all()
    success_url = '/news/'

class PostUpdateView(UpdateView):
    template_name='template/new_add.html'

    def get_object(self, **kwargs):
        id = self.kwargs.get('pk')
        return Post.objects.get(pk=id)
